import { NestFactory } from "@nestjs/core";
import { SwaggerModule, DocumentBuilder } from "@nestjs/swagger";
import { AppModule } from "./app/app.module";
import { Logger } from "@nestjs/common";
import { environment } from "./environments/environment";
async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    logger: environment.logLevel
  });
  app.setGlobalPrefix("api");
  const logger = await app.resolve(Logger);
  const port = process.env.port || environment.serverPort;
  if (environment.type === "develop") {
    const options = new DocumentBuilder().setTitle("Power Comp").build();
    const document = SwaggerModule.createDocument(app, options);
    SwaggerModule.setup("api", app, document);
  }
  await app.listen(port, () => {
    logger.log("Listening at http://localhost:" + port);
  });
}

bootstrap();
