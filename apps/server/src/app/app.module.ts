import "@nestjs/platform-socket.io";
import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { ConfigModule } from "@nestjs/config";
import { ServerApiModule } from "@pc/server-api";
import { CompetitionManagerModule } from "@pc/server/competition-manager";
import { SocketModule } from "@pc/server/socket";
import configuration from "./configuration";
import { environment } from "../environments/environment";
import { ServeStaticModule } from "@nestjs/serve-static";
import { join } from "path";

const imports = [
  ConfigModule.forRoot({
    load: [configuration]
  }),
  TypeOrmModule.forRoot({
    type: "sqlite",
    synchronize: true,
    database: process.env.DATABASE_NAME || environment.defaultDatabase,
    autoLoadEntities: true
  }),
  ServerApiModule,
  CompetitionManagerModule,
  SocketModule
];
if (environment.type === "standalone") {
  imports.push(
    ServeStaticModule.forRoot({
      rootPath: join(process.cwd(), "client"),
      renderPath: "*",
      exclude: ["/api/(.*)"]
    })
  );
}
@Module({
  imports,
  controllers: [],
  providers: []
})
export class AppModule {}
