import { IEnvironment } from "./IEnvironment";

export const defaultEnvironment: IEnvironment = {
  type: "develop",
  logLevel: ["error", "warn", "log", "debug"],
  serverPort: 3333,
  defaultDatabase: "DB/db.sqlite"
};
