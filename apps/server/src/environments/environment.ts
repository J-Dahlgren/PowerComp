import { IEnvironment } from "./IEnvironment";

import { defaultEnvironment } from "./environment.default";

export const environment: IEnvironment = {
  ...defaultEnvironment,
  logLevel: ["error", "warn", "log", "debug", "verbose"],
  defaultDatabase: ":memory:"
};
