import { LogLevel } from "@nestjs/common";
import { Environment } from "@pc/shared/env";
export interface IEnvironment {
  type: Environment;
  logLevel: LogLevel[];
  serverPort: number;
  defaultDatabase: string;
}
