import { Component, OnInit } from "@angular/core";
import { LogService, UiLogger } from "@pc/ui/logger";
import { SocketService } from "@pc/ui/socket";
import { navItems } from "./list-items";
import { LanguageService } from "@pc/ui/language";
import { TranslateService } from "@ngx-translate/core";
import { switchMap } from "rxjs/operators";
import { SnackBarService, SnackbarColor } from "@pc/ui/material";
import { of, combineLatest } from "rxjs";

@Component({
  selector: "power-comp-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent implements OnInit {
  logger: UiLogger;
  constructor(
    logFactory: LogService,
    private socket: SocketService,
    private languageService: LanguageService, // Protection if language selector isn't used
    private snackBar: SnackBarService,
    private translate: TranslateService
  ) {
    this.logger = logFactory.create("AppComponent");
  }
  title = "Power Comp";
  navItems = navItems;
  ngOnInit() {
    this.socket
      .select("connected")
      .pipe(
        switchMap(connected =>
          combineLatest([
            this.translate.get(
              `socket.${connected ? "connected" : "disconnected"}`
            ),
            of((connected ? "primary" : "warn") as SnackbarColor),
            of(connected ? 2500 : undefined)
          ])
        )
      )
      .subscribe(([text, color, duration]) =>
        this.snackBar.open(text, color, duration)
      );
  }
}
