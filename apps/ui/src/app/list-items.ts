import { NavItem } from "@pc/ui/menu";

export const navItems: NavItem[] = [
  {
    displayName: "admin",
    iconName: "cog",
    route: "",
    children: [
      {
        displayName: "admin.competition",
        iconName: "",
        route: "/competition"
      },
      {
        displayName: "admin.platforms",
        iconName: "",
        route: "/platform"
      },
      {
        displayName: "admin.groups",
        iconName: "",
        route: "/group"
      },
      {
        displayName: "Weight Categories",
        iconName: "",
        route: "/weightcategory"
      },
      {
        displayName: "admin.lifters",
        iconName: "",
        route: "/lifter"
      }
    ]
  },
  {
    displayName: "display",
    iconName: "monitor",
    children: [
      {
        displayName: "display.main-result",
        iconName: "",
        route: "/resultboard"
      },
      {
        displayName: "display.lift-order",
        iconName: "",
        route: "/liftorder"
      },
      {
        displayName: "display.attempt-board",
        iconName: "",
        route: "/attemptboardpublic"
      }
    ]
  },
  {
    displayName: "Result",
    iconName: "podium",
    route: "/result"
  },
  {
    displayName: "secretariat",
    iconName: "tablet-dashboard",
    route: "/secretariat"
  },
  {
    displayName: "timekeeper",
    iconName: "timer",
    route: "/timekeeper"
  }
];
