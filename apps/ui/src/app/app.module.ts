import { BrowserModule, DomSanitizer } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { MatIconRegistry, MatIconModule } from "@angular/material/icon";
import { AppComponent } from "./app.component";
import { RouterModule } from "@angular/router";
import { UiLoggerModule } from "@pc/ui/logger";
import { environment } from "../environments/environment";
import { HttpClientModule, HttpClient } from "@angular/common/http";
import { ApiModule } from "@pc/ui/api";
import { UiSocketModule } from "@pc/ui/socket";
import { UiMaterialModule } from "@pc/ui/material";
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { LanguageModule } from "@pc/ui/language";
import { NgxWebstorageModule } from "ngx-webstorage";
import { MenuModule } from "@pc/ui/menu";
import { DialogModule } from "@pc/ui/dialog";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { AdminModule } from "@pc/ui/admin";
import { routes } from "./routes";

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NgxWebstorageModule.forRoot(),
    UiLoggerModule.forRoot(environment.logLevel),
    ApiModule.forRoot(500),
    UiSocketModule.forRoot({
      port: environment.socketPort,
      host: window.location.hostname
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    LanguageModule.forRoot([
      { language: "en", flag: "us" },
      { language: "sv", flag: "se" }
    ]),
    RouterModule.forRoot(routes, {
      onSameUrlNavigation: "reload",
      useHash: environment.type === "develop"
    }),
    AdminModule,
    UiMaterialModule,
    MenuModule,
    DialogModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(matIconRegistry: MatIconRegistry, domSanitizer: DomSanitizer) {
    matIconRegistry.addSvgIconSet(
      domSanitizer.bypassSecurityTrustResourceUrl("./assets/mdi.svg")
    );
  }
}
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
