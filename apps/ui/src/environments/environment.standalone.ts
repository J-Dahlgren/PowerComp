import { IEnvironment } from "./IEnvironment";
import { defaultEnvironment } from "./environment.default";

export const environment: IEnvironment = {
  ...defaultEnvironment,
  type: "standalone"
};
