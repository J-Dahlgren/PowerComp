import { IEnvironment } from "./IEnvironment";
import { LogLevel } from "@pc/ui/logger";

export const defaultEnvironment: IEnvironment = {
  type: "develop",
  logLevel: LogLevel.log,
  socketPort: 80
};
