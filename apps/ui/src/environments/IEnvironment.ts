import { LogLevel } from "@pc/ui/logger";
import { Environment } from "@pc/shared/env";
export interface IEnvironment {
  type: Environment;
  logLevel: LogLevel;
  socketPort: number;
}
