if (process.platform !== "win32") {
  //throw new Error(`Script only allowed to run on win32`);
}
const shell = require("shelljs");
const projectName = "standalone-power-comp";
const buildPath = `dist/apps/${projectName}`;
const binPath = `bin/${projectName}`;
shell.echo("Building Standalone PowerComp");
shell.mkdir("-p", "dist/apps/standalone-power-comp");
shell.rm("-rf", `${buildPath}/*`);
shell.mkdir(`${buildPath}/client`);
shell.echo("Build .exe bundle");
shell.exec(
  `npx pkg dist/apps/server/main.js -t node12-win --output ${buildPath}/powercomp.exe`
);
shell.echo("Copy needed files to output folder");
shell.cp("apps/standalone-power-comp/node_sqlite3.node", `${buildPath}`);
shell.cp("-r", "dist/apps/ui/*", `${buildPath}/client`);
shell.mkdir("-p", binPath);
shell.cp("-r", `${buildPath}/*`, `bin/${projectName}`);
shell.echo("Create url");
shell
  .ShellString(
    `[{000214A0-0000-0000-C000-000000000046}]
Prop3=19,2
[InternetShortcut]
IDList=
URL=http://localhost/
HotKey=0`
  )
  .to(`${binPath}/power-comp.url`);
console.log("Done");
