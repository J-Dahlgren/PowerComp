import { NgModule } from "@angular/core";
import { RouterModule, Route } from "@angular/router";
import { CompetitionListComponent } from "./competition-list/competition-list.component";
import { CommonModule } from "@angular/common";
import { CompetitionEditComponent } from "./competition-edit/competition-edit.component";
const baseRoute = "competition";
export const adminRoutes: Route[] = [
  {
    path: baseRoute,
    component: CompetitionListComponent
  },
  {
    component: CompetitionEditComponent,
    path: `${baseRoute}/:id`
  }
];
@NgModule({
  imports: [CommonModule, RouterModule.forChild(adminRoutes)],
  exports: []
})
export class AdminRoutingModule {}
