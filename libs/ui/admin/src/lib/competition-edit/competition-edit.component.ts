import { Component, OnInit } from "@angular/core";
import { Router, Route, ActivatedRoute } from "@angular/router";
import { LogService, UiLogger } from "@pc/ui/logger";
import { CompetitionService } from "@pc/ui/api";
import { switchMap, map } from "rxjs/operators";
import { Observable } from "rxjs";
import { IEntity, ICompetition, IPlatform } from "@pc/shared/data-types";
import { RequestQueryBuilder } from "@nestjsx/crud-request";
import { CompetitionEditService } from "../competition-edit.service";
import { ModalService, PlatformDialogComponent } from "@pc/ui/dialog";
@Component({
  selector: "pc-competition-edit",
  templateUrl: "./competition-edit.component.html",
  styleUrls: ["./competition-edit.component.scss"],
  providers: [CompetitionEditService]
})
export class CompetitionEditComponent implements OnInit {
  logger: UiLogger;
  comp$: Observable<IEntity<ICompetition>>;

  constructor(
    private router: ActivatedRoute,
    logService: LogService,
    private competitionService: CompetitionService,
    private dialog: ModalService
  ) {
    const query = RequestQueryBuilder.create({
      join: [
        {
          field: "platforms"
        },
        { field: "platform.groups" }
      ]
    }).query();
    this.logger = logService.create("CompetitionEditComponent");
    this.comp$ = router.params.pipe(
      switchMap(params => competitionService.getAllRelations(+params.id))
    );
  }
  editPlatform(platform: IEntity<IPlatform>) {
    this.dialog.openEditModal(PlatformDialogComponent, {
      data: { id: platform.id },
      minWidth: "300px"
    });
  }
  ngOnInit(): void {}
}
