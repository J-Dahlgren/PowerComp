import { Component, OnInit } from "@angular/core";
import { GlobalEventService } from "@pc/ui/event-bus";
import { Observable } from "rxjs";
import { ICompetition, IEntity } from "@pc/shared/data-types";
import { Router, ActivatedRoute } from "@angular/router";
import { ModalService, CompetitionDialogComponent } from "@pc/ui/dialog";
import { CompetitionEditComponent } from "../competition-edit/competition-edit.component";
import { filter, map } from "rxjs/operators";
@Component({
  selector: "pc-competition-list",
  templateUrl: "./competition-list.component.html",
  styleUrls: ["./competition-list.component.scss"]
})
export class CompetitionListComponent implements OnInit {
  competitions$: Observable<IEntity<ICompetition>[]>;
  hoverClasses = ["mat-elevation-z8", "card"];
  constructor(
    private globalData: GlobalEventService,
    private modalService: ModalService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.competitions$ = globalData.on("competitions");
  }

  ngOnInit(): void {}
  delete(id: number) {}
  add() {
    this.modalService
      .openEditModal<ICompetition>(CompetitionDialogComponent)
      .afterClosed()
      .pipe(
        filter(r => !!r),
        map(r => r as IEntity<ICompetition>)
      )
      .subscribe(r => this.click(r.id));
  }
  click(id: number) {
    this.router.navigate([id], { relativeTo: this.route });
  }
}
