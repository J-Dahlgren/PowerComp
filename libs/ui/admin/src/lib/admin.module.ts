import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule, Route } from "@angular/router";
import { UiEventBusModule } from "@pc/ui/event-bus";
import { CompetitionListComponent } from "./competition-list/competition-list.component";
import { AdminRoutingModule } from "./admin-routin.module";
import { UiMaterialModule } from "@pc/ui/material";
import { CompetitionEditComponent } from "./competition-edit/competition-edit.component";
import { UiUtilModule } from "@pc/ui/util";
import { TranslateModule } from "@ngx-translate/core";
@NgModule({
  imports: [
    CommonModule,
    UiMaterialModule,
    AdminRoutingModule,
    TranslateModule,
    RouterModule,
    UiUtilModule,
    UiEventBusModule
  ],
  exports: [AdminRoutingModule],
  declarations: [CompetitionListComponent, CompetitionEditComponent]
})
export class AdminModule {}
