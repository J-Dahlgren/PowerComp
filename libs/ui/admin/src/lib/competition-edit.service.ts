import { Injectable } from "@angular/core";
import { ProtectedStore } from "@pc/shared/util";
import { IGroup, IPlatform, ILifter, IEntity } from "@pc/shared/data-types";
export class CompetitionEditData {
  platforms: IEntity<IPlatform>[] = [];
  groups: IEntity<IGroup>[] = [];
  lifters: IEntity<ILifter>[] = [];
}
@Injectable()
export class CompetitionEditService extends ProtectedStore<
  CompetitionEditData
> {
  constructor() {
    super(new CompetitionEditData());
  }
}
