import { NgModule, ModuleWithProviders } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SocketService } from "./socket.service";
import { UiEventBusModule } from "@pc/ui/event-bus";
export interface SocketModuleOpts {
  host: string;
  port: number;
}
@NgModule({
  imports: [CommonModule, UiEventBusModule]
})
export class UiSocketModule {
  static forRoot(opts: SocketModuleOpts): ModuleWithProviders {
    return {
      ngModule: UiSocketModule,
      providers: [
        {
          provide: "SOCKET_PORT",
          useValue: opts.port
        },
        {
          provide: "SOCKET_HOST",
          useValue: opts.host
        },
        SocketService
      ]
    };
  }
}
