import { async, TestBed } from "@angular/core/testing";
import { UiSocketModule } from "./ui-socket.module";

describe("UiSocketModule", () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [UiSocketModule]
    }).compileComponents();
  }));

  it("should create", () => {
    expect(UiSocketModule).toBeDefined();
  });
});
