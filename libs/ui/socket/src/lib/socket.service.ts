import { Injectable, Inject, Optional } from "@angular/core";
import { filter, map } from "rxjs/operators";
import { PlatformEventService, GlobalEventService } from "@pc/ui/event-bus";
import { LogService } from "@pc/ui/logger";
import {
  JOIN,
  LEAVE,
  PlatformDataEvents,
  GlobalEvents,
  globalEvent,
  SYNC,
  GLOBAL_DATA,
  PLATFORM_DATA_EVENT,
  platformEvent,
  PLATFORM_SERVER_EVENT,
  PlatformServerEvents,
  PLATFORM_CLIENT_EVENT
} from "@pc/shared/events";
import { IEvent, IRoomEvent, RoomBus } from "@pc/shared/util";
import { BaseSocketService, SocketState } from "./base-socket.service";
@Injectable({
  providedIn: "root"
})
export class SocketService extends BaseSocketService<SocketState> {
  constructor(
    globalEventService: GlobalEventService,
    platformEventService: PlatformEventService,
    @Inject("SOCKET_HOST") host: string,
    @Inject("SOCKET_PORT") port: number,
    logFactory: LogService
  ) {
    super(logFactory.create("SocketService"), host, port, { connected: false });
    this.fromEvent$<IEvent<GlobalEvents>>(GLOBAL_DATA).subscribe(e =>
      globalEventService.emit(e.type, e.payload)
    );
    this.fromEvent$<IEvent<RoomBus<PlatformDataEvents>>>(
      PLATFORM_DATA_EVENT
    ).subscribe(e =>
      platformEventService.dataEvents.emitIn(
        e.type,
        e.payload.room,
        e.payload.data
      )
    );

    this.fromEvent$<IEvent<RoomBus<PlatformServerEvents>>>(
      PLATFORM_SERVER_EVENT
    ).subscribe(event =>
      platformEventService.serverEvents.emit(event.type, event.payload)
    );
    platformEventService.clientEvents
      .onAny()
      .subscribe(event => this.emit(PLATFORM_CLIENT_EVENT, event));
    this.select("connected")
      .pipe(filter(c => c))
      .subscribe(() => this.emit(globalEvent(SYNC)));
  }
  joinPlatform(platformId: number) {
    this.emit(platformEvent(JOIN), platformId.toString());
  }
  leavePlatform(platformId: number) {
    this.emit(platformEvent(LEAVE), platformId.toString());
  }
  globalEvent$<K extends keyof GlobalEvents>(event: K) {
    return this.fromEvent$<GlobalEvents[K]>(globalEvent(event));
  }

  platformEvents$<K extends keyof PlatformDataEvents>(event: K) {
    return this.fromEvent$<IRoomEvent<PlatformDataEvents[K]>>(
      platformEvent(event)
    );
  }
  platformEventData$<K extends keyof PlatformDataEvents>(
    event: K,
    room: () => string
  ) {
    return this._platformEvent$(event, room).pipe(map(e => e.data));
  }
  private _platformEvent$<K extends keyof PlatformDataEvents>(
    event: K,
    room: () => string
  ) {
    return this.fromEvent$<IRoomEvent<PlatformDataEvents[K]>>(
      platformEvent(event)
    ).pipe(filter(e => e.room === room()));
  }
}
