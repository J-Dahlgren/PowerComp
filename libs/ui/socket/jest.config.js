module.exports = {
  name: "ui-socket",
  preset: "../../../jest.config.js",
  coverageDirectory: "../../../coverage/libs/ui/socket",
  snapshotSerializers: [
    "jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js",
    "jest-preset-angular/build/AngularSnapshotSerializer.js",
    "jest-preset-angular/build/HTMLCommentSerializer.js"
  ]
};
