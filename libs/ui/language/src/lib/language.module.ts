import { NgModule, ModuleWithProviders } from "@angular/core";
import { CommonModule } from "@angular/common";
import { LanguageService } from "./language.service";
import { UiMaterialModule } from "@pc/ui/material";
import { LanguageSelectorComponent } from "./language-selector/language-selector.component";
import { LanguageCode } from "./language-code";
@NgModule({
  imports: [CommonModule, UiMaterialModule],
  declarations: [LanguageSelectorComponent],
  exports: [LanguageSelectorComponent]
})
export class LanguageModule {
  static forRoot(codes: LanguageCode[]): ModuleWithProviders {
    return {
      ngModule: LanguageModule,
      providers: [
        {
          provide: "COUNTRY_CODES",
          useValue: codes
        },
        LanguageService
      ]
    };
  }
}
