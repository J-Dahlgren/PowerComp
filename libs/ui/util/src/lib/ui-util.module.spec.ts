import { async, TestBed } from "@angular/core/testing";
import { UiUtilModule } from "./ui-util.module";

describe("UiUtilModule", () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [UiUtilModule]
    }).compileComponents();
  }));

  it("should create", () => {
    expect(UiUtilModule).toBeDefined();
  });
});
