import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HoverClassDirective } from "./hover-class.directive";

@NgModule({
  imports: [CommonModule],
  declarations: [HoverClassDirective],
  exports: [HoverClassDirective]
})
export class UiUtilModule {}
