import { Injectable, Optional, Inject } from "@angular/core";
import { logLevelToken } from "./log-level.token";
import { LogLevel } from "./log-level";
import { UiLogger } from "./ui-logger.service";
import { Subject } from "rxjs";
import { LogEvent } from "./log-event";
import { RollingList } from "@pc/shared/util";
@Injectable({ providedIn: "root" })
export class LogService {
  private reportStream = new Subject<LogEvent>();
  private logBuffer = new RollingList<LogEvent>(100);
  constructor(@Optional() @Inject(logLevelToken) private logLevel: LogLevel) {
    if (!logLevel) {
      this.logLevel = LogLevel.off;
    }
    this.create("LogServiceFactory");
    this.reportStream.subscribe(event => this.logBuffer.add(event));
  }
  get log$() {
    return this.reportStream.asObservable();
  }
  completeLog() {
    return this.logBuffer.getList();
  }
  create(context: string) {
    return new UiLogger(this.logLevel, this.reportStream)
      .setContext(context)
      .trace("Created");
  }
}
