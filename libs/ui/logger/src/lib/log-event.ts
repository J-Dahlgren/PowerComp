import { LogLevel } from "./log-level";

export interface LogEvent {
  timestamp: number;
  logLevel: LogLevel;
  context: string;
  message: string;
  data: any;
}
