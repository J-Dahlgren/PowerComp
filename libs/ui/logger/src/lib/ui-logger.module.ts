import { NgModule, ModuleWithProviders } from "@angular/core";
import { CommonModule } from "@angular/common";
import { UiLogger } from "./ui-logger.service";
import { LogLevel } from "./log-level";
import { logLevelToken } from "./log-level.token";
import { LogService } from "./log-factory.service";

@NgModule({
  imports: [CommonModule]
})
export class UiLoggerModule {
  static forRoot(logLevel: LogLevel): ModuleWithProviders {
    return {
      ngModule: UiLoggerModule,
      providers: [
        LogService,
        {
          provide: logLevelToken,
          useValue: logLevel
        }
      ]
    };
  }
}
