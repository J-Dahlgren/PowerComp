export enum LogLevel {
  off,
  error,
  warn,
  log,
  debug,
  trace
}
