import { async, TestBed } from "@angular/core/testing";
import { UiEventBusModule } from "./ui-event-bus.module";

describe("UiEventBusModule", () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [UiEventBusModule]
    }).compileComponents();
  }));

  it("should create", () => {
    expect(UiEventBusModule).toBeDefined();
  });
});
