import { Injectable } from "@angular/core";
import {
  PlatformDataEvents,
  PlatformClientEvents,
  PlatformServerEvents
} from "@pc/shared/events";
import { RoomEventBus } from "@pc/shared/util";
import { LogService, UiLogger } from "@pc/ui/logger";
@Injectable({ providedIn: "root" })
export class PlatformEventService {
  private logger: UiLogger;
  public readonly clientEvents = new RoomEventBus<PlatformClientEvents>();
  public readonly serverEvents = new RoomEventBus<PlatformServerEvents>();
  public readonly dataEvents = new RoomEventBus<PlatformDataEvents>();
  constructor(logService: LogService) {
    this.logger = logService.create("PlatformEventService");

    // this.onAny().subscribe(event =>
    //   this.logger.trace(event.type, event.payload)
    // );
  }
}
