import { Injectable } from "@angular/core";
import { GlobalEvents } from "@pc/shared/events";
import { EventBus, ProtectedStore, DataBus } from "@pc/shared/util";
import { LogService, UiLogger } from "@pc/ui/logger";
import { skip } from "rxjs/operators";
@Injectable({ providedIn: "root" })
export class GlobalEventService extends DataBus<GlobalEvents> {
  private logger: UiLogger;
  constructor(logService: LogService) {
    super({ competitions: [], platforms: [] });
    this.logger = logService.create("GlobalEventService");
    this.onAny()
      .pipe(skip(2))
      .subscribe(event => this.logger.trace(event.type, event.payload));
  }
}
