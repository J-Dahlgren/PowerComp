import { Component, OnInit, Input } from "@angular/core";
import { NavigationService } from "../navigation.service";
import { Observable } from "rxjs";
import { HeaderService } from "../header.service";

@Component({
  selector: "pc-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.scss"]
})
export class HeaderComponent implements OnInit {
  visible$: Observable<boolean>;
  @Input() appName: string = "";
  constructor(
    headerService: HeaderService,
    private navService: NavigationService
  ) {
    this.visible$ = headerService.select("visible");
  }

  ngOnInit(): void {}
  toggleExpanded() {
    this.navService.toggle();
  }
}
