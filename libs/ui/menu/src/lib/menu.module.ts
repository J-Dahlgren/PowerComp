import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SideNavComponent } from "./side-nav/side-nav.component";
import { ListItemComponent } from "./list-item/list-item.component";
import { NavigationService } from "./navigation.service";
import { UiMaterialModule } from "@pc/ui/material";
import { RouterModule } from "@angular/router";
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { HttpClient } from "@angular/common/http";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { HeaderComponent } from "./header/header.component";
import { HeaderService } from "./header.service";
import { MenuShellComponent } from "./menu-shell/menu-shell.component";

@NgModule({
  imports: [
    RouterModule,
    CommonModule,
    UiMaterialModule,
    TranslateModule,
    RouterModule
  ],
  declarations: [
    SideNavComponent,
    ListItemComponent,
    HeaderComponent,
    MenuShellComponent
  ],
  providers: [NavigationService, HeaderService],
  exports: [SideNavComponent, ListItemComponent, HeaderComponent]
})
export class MenuModule {}
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
