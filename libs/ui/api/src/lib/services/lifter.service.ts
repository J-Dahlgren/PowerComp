import { Injectable, Inject } from "@angular/core";
import { BaseApiService } from "./base-api.service";
import { ILifter } from "@pc/shared/data-types";
import { LogService } from "@pc/ui/logger";
import { HttpClient } from "@angular/common/http";
import { MIN_DELAY_TIME } from "../token";
@Injectable({ providedIn: "root" })
export class LifterService extends BaseApiService<ILifter> {
  constructor(
    @Inject(MIN_DELAY_TIME) protected minDelay: number,
    logService: LogService,
    protected http: HttpClient
  ) {
    super("Lifter", logService.create("LifterService"));
  }
}
