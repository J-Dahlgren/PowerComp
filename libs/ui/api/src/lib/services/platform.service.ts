import { Injectable, Inject } from "@angular/core";
import { BaseApiService } from "./base-api.service";
import { IPlatform } from "@pc/shared/data-types";
import { LogService } from "@pc/ui/logger";
import { HttpClient } from "@angular/common/http";
import { MIN_DELAY_TIME } from "../token";
@Injectable({ providedIn: "root" })
export class PlatformService extends BaseApiService<IPlatform> {
  constructor(
    @Inject(MIN_DELAY_TIME) protected minDelay: number,
    logService: LogService,
    protected http: HttpClient
  ) {
    super("Platform", logService.create("PlatformService"));
  }
}
