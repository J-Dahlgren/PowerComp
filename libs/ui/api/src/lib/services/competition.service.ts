import { Injectable, Inject } from "@angular/core";
import { BaseApiService } from "./base-api.service";
import { ICompetition } from "@pc/shared/data-types";
import { LogService } from "@pc/ui/logger";
import { HttpClient } from "@angular/common/http";
import { MIN_DELAY_TIME } from "../token";
import { RequestQueryBuilder } from "@nestjsx/crud-request";
@Injectable({ providedIn: "root" })
export class CompetitionService extends BaseApiService<ICompetition> {
  constructor(
    @Inject(MIN_DELAY_TIME) protected minDelay: number,
    logService: LogService,
    protected http: HttpClient
  ) {
    super("competition", logService.create("CompetitionService"));
  }
  getAllRelations(id: number | string) {
    const query = RequestQueryBuilder.create()
      .setJoin({ field: "platforms" })
      .setJoin({ field: "groups" })
      .setJoin({ field: "lifters" })
      .query();

    return this.get(id, query);
  }
}
