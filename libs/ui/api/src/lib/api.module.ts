import { NgModule, ModuleWithProviders } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { CommonModule } from "@angular/common";
import { MIN_DELAY_TIME } from "./token";

@NgModule({
  imports: [CommonModule, HttpClientModule]
})
export class ApiModule {
  static forRoot(forcedDelayMillis = 0): ModuleWithProviders {
    return {
      ngModule: ApiModule,
      providers: [{ provide: MIN_DELAY_TIME, useValue: forcedDelayMillis }]
    };
  }
}
