import { Component, OnInit, Inject } from "@angular/core";
import { EditDialog } from "../generic-edit-dialog/edit-dialog";
import {
  IPlatform,
  IEntity,
  IPlatformWeights,
  IWeightPlate,
  defualtPlatformWeights
} from "@pc/shared/data-types";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { DialogOptions } from "../generic-edit-dialog";
import { CompetitionDialogComponent } from "..";
import { CompetitionService, PlatformService } from "@pc/ui/api";
import { LogService } from "@pc/ui/logger";
import { Observable } from "rxjs";
import { FormControl, Validators, FormGroup, FormArray } from "@angular/forms";
import { tap } from "rxjs/operators";

@Component({
  selector: "pc-platform-dialog",
  templateUrl: "./platform-dialog.component.html",
  styleUrls: ["./platform-dialog.component.scss"]
})
export class PlatformDialogComponent extends EditDialog<IPlatform> {
  constructor(
    @Inject(MAT_DIALOG_DATA) public config: DialogOptions,
    public dialogRef: MatDialogRef<PlatformDialogComponent>,
    private platformService: PlatformService,
    logFactory: LogService
  ) {
    super(platformService, logFactory.create("PlatformDialogComponent"));
    config.title = config.title || "platform";
  }
  getData$(id: number): Observable<IEntity<IPlatform>> {
    return this.platformService.get(id).pipe(tap(data => {}));
  }
  buildForm(data?: IEntity<IPlatform>): void {
    this.form.addControl("competitionId", new FormControl());
    this.form.addControl(
      "name",
      new FormControl("", [Validators.required, Validators.minLength(1)])
    );
    this.form.addControl("weights", this.createWeights(defualtPlatformWeights));
  }
  get plates() {
    return this.form.get("weights")?.get("plates") as FormArray;
  }
  createWeights(weights: IPlatformWeights) {
    return new FormGroup({
      collarWeight: new FormControl("", [Validators.required]),
      plates: new FormArray(this.createPlates(weights.plates))
    });
  }
  createPlates(plates: IWeightPlate[]) {
    return plates.map(
      plate =>
        new FormGroup({
          weight: new FormControl(plate.weight, [
            Validators.required,
            Validators.min(0)
          ]),
          quantity: new FormControl(plate.quantity)
        })
    );
  }
}
