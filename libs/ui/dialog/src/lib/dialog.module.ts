import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { UiMaterialModule } from "@pc/ui/material";
import { ReactiveFormsModule } from "@angular/forms";
import { CompetitionDialogComponent } from "./competition-dialog/competition-dialog.component";
import { GenericEditDialogComponent } from "./generic-edit-dialog/generic-edit-dialog.component";
import { ConfirmDialogComponent } from "./confirm-dialog/confirm-dialog.component";
import { TranslateModule } from "@ngx-translate/core";
import { PlatformDialogComponent } from "./platform-dialog/platform-dialog.component";
@NgModule({
  imports: [
    CommonModule,
    UiMaterialModule,
    ReactiveFormsModule,
    TranslateModule
  ],
  providers: [],
  declarations: [
    GenericEditDialogComponent,
    CompetitionDialogComponent,
    ConfirmDialogComponent,
    PlatformDialogComponent
  ],
  exports: [PlatformDialogComponent]
})
export class DialogModule {}
