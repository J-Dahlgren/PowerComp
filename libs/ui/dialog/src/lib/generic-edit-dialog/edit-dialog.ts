import { Observable, Subject } from "rxjs";
import { IEntity } from "@pc/shared/data-types";
import { OnInit } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { UiLogger } from "@pc/ui/logger";
import { IApiService } from "@pc/ui/api";
import { ProtectedStore } from "@pc/shared/util";
import { DialogOptions } from "./dialog-options";
import { MatDialogRef } from "@angular/material/dialog";
export interface DialogState {
  loading: boolean;
  saving: boolean;
}
export abstract class EditDialog<T extends object>
  extends ProtectedStore<DialogState>
  implements OnInit {
  form: FormGroup = new FormGroup({});
  error$ = new Subject<any>();
  abstract config: DialogOptions;
  abstract dialogRef: MatDialogRef<EditDialog<T>, IEntity<T>>;
  constructor(public readonly api: IApiService<T>, protected logger: UiLogger) {
    super({ loading: false, saving: false });
  }
  abstract getData$(id: number): Observable<IEntity<T>>;
  save() {
    if (this.config.id) {
      return this.api.patch(this.config.id, this.form.getRawValue());
    }
    return this.api.create(this.form.getRawValue());
  }
  ngOnInit() {
    this.buildForm();
    if (this.config.id) {
      this.set("loading", true);
      this.getData$(this.config.id).subscribe(
        data => {
          this.buildForm(data);
          this.form.patchValue(data);
        },
        e => {
          this.error$.next(e);
          this.dialogRef.close();
        },
        () => this.set("loading", false)
      );
    } else {
    }
  }
  public get model() {
    return this;
  }
  abstract buildForm(data?: IEntity<T>): void;
}
