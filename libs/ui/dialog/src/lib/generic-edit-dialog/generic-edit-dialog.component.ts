import { Component, Input, OnInit } from "@angular/core";
import { Observable, Subject, of, merge, concat } from "rxjs";
import { FormGroup } from "@angular/forms";
import { isEqual } from "lodash";
import { EditDialog } from "./edit-dialog";
import {
  tap,
  mapTo,
  exhaustMap,
  catchError,
  switchMap,
  map
} from "rxjs/operators";
import { SnackBarService } from "@pc/ui/material";
@Component({
  selector: "pc-generic-edit-dialog",
  styleUrls: ["./generic-edit-dialog.component.scss"],
  templateUrl: "./generic-edit-dialog.component.html"
})
export class GenericEditDialogComponent<T extends object> implements OnInit {
  @Input() parent!: EditDialog<T>;
  loading$!: Observable<boolean>;
  submitClick$ = new Subject<void>();
  form!: FormGroup;
  saving$!: Observable<boolean>;
  constructor(private snackBar: SnackBarService) {}
  ngOnInit() {
    this.form = this.parent.form;
    this.loading$ = this.parent.select("loading");
    this.parent.error$.subscribe(e =>
      this.snackBar.open(e?.message, "warn", 2500)
    );
    this.submitClick$.subscribe(() => console.log("clicked"));
    this.saving$ = this.submitClick$.pipe(
      switchMap(() =>
        this.parent.save().pipe(
          tap(
            entity => {
              this.snackBar.open("Saved", "accent", 2500);
              this.parent.dialogRef.close(entity);
            },
            e => this.snackBar.open(e?.message, "warn", 2500)
          ),
          map(() => false)
        )
      )
    );
  }
}
