import { Component, Inject } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { ICompetition, IEntity } from "@pc/shared/data-types";
import { EditDialog } from "../generic-edit-dialog/edit-dialog";
import { CompetitionService } from "@pc/ui/api";
import { Observable, of } from "rxjs";
import { DialogOptions } from "../generic-edit-dialog/dialog-options";
import { Validators, FormControl } from "@angular/forms";
import { LogService } from "@pc/ui/logger";
@Component({
  selector: "pc-competition-dialog",
  templateUrl: "./competition-dialog.component.html",
  styleUrls: ["./competition-dialog.component.scss"]
})
export class CompetitionDialogComponent extends EditDialog<ICompetition> {
  constructor(
    @Inject(MAT_DIALOG_DATA) public config: DialogOptions,
    public dialogRef: MatDialogRef<CompetitionDialogComponent>,
    private competitionService: CompetitionService,
    logFactory: LogService
  ) {
    super(competitionService, logFactory.create("CompetitionDialogComponent"));
    config.title = config.title || "competition";
  }
  getData$(id: number): Observable<IEntity<ICompetition>> {
    return this.competitionService.get(id);
  }
  buildForm() {
    this.form.addControl(
      "name",
      new FormControl("", [Validators.required, Validators.minLength(1)])
    );
    this.form.addControl("city", new FormControl(""));
    this.form.addControl("location", new FormControl(""));
  }
}
