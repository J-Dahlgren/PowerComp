export * from "./modal.service";
export * from "./dialog.module";
export * from "./modal-data";
export * from "./competition-dialog/competition-dialog.component";
export * from "./generic-edit-dialog/generic-edit-dialog.component";
export * from "./platform-dialog/platform-dialog.component";
