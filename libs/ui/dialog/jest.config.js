module.exports = {
  name: "ui-dialog",
  preset: "../../../jest.config.js",
  coverageDirectory: "../../../coverage/libs/ui/dialog",
  snapshotSerializers: [
    "jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js",
    "jest-preset-angular/build/AngularSnapshotSerializer.js",
    "jest-preset-angular/build/HTMLCommentSerializer.js"
  ]
};
