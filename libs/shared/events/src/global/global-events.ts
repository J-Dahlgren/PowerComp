import { ICompetition, IPlatform, IEntity } from "@pc/shared/data-types";

export class GlobalEvents {
  competitions: IEntity<ICompetition>[] = [];
  platforms: IEntity<IPlatform>[] = [];
}
export const globalDataObject = new GlobalEvents();
export const GLOBAL_DATA = "GLOBAL_DATA";
