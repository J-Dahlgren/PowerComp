import { ICompetition, IPlatform } from "@pc/shared/data-types";

export class CompetitionData {
  competition?: ICompetition | null = null;
  platforms: IPlatform[] = [];
}
