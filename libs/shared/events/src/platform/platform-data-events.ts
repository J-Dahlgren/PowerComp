import { PlatformData } from "./platform-data";

export interface PlatformDataEvents {
  data: PlatformData;
}
export interface PlatformClientEvents {
  activeGroup: number;
}
export interface PlatformServerEvents {
  displayDecision: number;
}
