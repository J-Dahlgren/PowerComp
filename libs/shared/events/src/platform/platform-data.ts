import { IEntity, IAttemptInfo } from "@pc/shared/data-types";
import { platformEvent } from "../event-name-functions";

export class PlatformData {
  activeGroupId: number | null = null;
  currentLifter: IEntity<IAttemptInfo> | null = null;
  nextLifter: IEntity<IAttemptInfo> | null = null;
  ranking: any[] = [];
}
export const PLATFORM_DATA_EVENT = "PLATFORM_DATA_EVENT";
export const PLATFORM_CLIENT_EVENT = "PLATFORM_CLIENT_EVENT";
export const PLATFORM_SERVER_EVENT = "PLATFORM_SERVER_EVENT";
