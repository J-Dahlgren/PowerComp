import { GLOBAL_PREFIX, PLATFORM_PREFIX } from "./prefixes";

export function platformEvent(eventName: string) {
  return `${PLATFORM_PREFIX}${eventName}`;
}
export function globalEvent(eventName: string) {
  return `${GLOBAL_PREFIX}${eventName}`;
}
