import { IEntity } from "./IEntity";
import { ILifter } from "./ILifter";

export interface IGroup {
  name: string;
  weighInTime?: Date;
  competitionTime?: Date;
  lifters: IEntity<ILifter>[];
}
