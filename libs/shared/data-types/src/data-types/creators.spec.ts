import { createILifter, createIAttempt } from "./creators";
import { LiftStatus } from "./lifts";

describe("creator functions", () => {
  test("createILifter()", () => {
    expect(createILifter().firstname).toBe("");
  });
  test("createIAttempt()", () => {
    expect(createIAttempt({ status: LiftStatus.SUCCESFUL }).status).toBe(
      LiftStatus.SUCCESFUL
    );
  });
});
