import { ILifter } from "./ILifter";
import { ILifts, IAttempt, LiftStatus } from "./lifts";
import { Gender } from "./IPerson";

export function createILifter(initial: Partial<ILifter> = {}): ILifter {
  return {
    firstname: "",
    lastname: "",
    lot: 0,
    competitionId: 0,
    gender: Gender.male,
    lifts: createILifts(),
    ...initial
  };
}
export function createILifts(initial: Partial<ILifts> = {}): ILifts {
  return { squat: [], bench: [], deadlift: [], ...initial };
}
export function createIAttempt(initial: Partial<IAttempt> = {}): IAttempt {
  return { status: LiftStatus.NOT_ATTEMPTED, ...initial };
}
