import { IPerson, Gender } from "./IPerson";
import { ILifts } from "./lifts";
export interface ILifter extends IPerson {
  lot: number;
  competitionId: number;
  groupId?: number;
  lifts: ILifts;
}
