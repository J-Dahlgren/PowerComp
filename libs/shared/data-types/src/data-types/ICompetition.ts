import { IPlatform } from "./IPlatform";
import { IGroup } from "./IGroup";
import { ILifter } from "./ILifter";
import { IEntity } from "./IEntity";

export interface ICompetition {
  name: string;
  city?: string;
  location?: string;
  platforms: IEntity<IPlatform>[];
  groups: IEntity<IGroup>[];
  lifters: IEntity<ILifter>[];
}
