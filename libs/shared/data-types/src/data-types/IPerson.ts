export enum Gender {
  male = "m",
  female = "f"
}
export interface IPerson {
  license?: string;
  firstname: string;
  lastname: string;
  gender: Gender;
  born?: Date;
}
