import { IGroup } from "./IGroup";
import { IEntity } from "./IEntity";

export interface IPlatform {
  name: string;
  competitionId: number;
  weights: IPlatformWeights;
  groups?: IEntity<IGroup>[];
}

export interface IPlatformWeights {
  collarWeight: number;
  plates: IWeightPlate[];
}

export interface IWeightPlate {
  weight: number;
  quantity: number;
}
export const defualtPlatformWeights: IPlatformWeights = {
  collarWeight: 2.5,
  plates: [
    { weight: 25, quantity: 8 },
    { weight: 20, quantity: 1 },
    { weight: 15, quantity: 1 },
    { weight: 10, quantity: 1 },
    { weight: 5, quantity: 1 },
    { weight: 2.5, quantity: 1 },
    { weight: 1.25, quantity: 1 },
    { weight: 0.5, quantity: 1 },
    { weight: 0.25, quantity: 2 }
  ]
};
