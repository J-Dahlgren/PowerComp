import { LiftStatus, ILifts, requestedWeight, IAttempt } from "./lifts";
import { AttemptInfo, attemptInfo } from "./attempt-info";
import { createIAttempt } from "./creators";

describe("Attempt", () => {
  test("requestedWeight()", () => {
    const attempt: IAttempt = createIAttempt();
    expect(requestedWeight(attempt)).toBeUndefined();
    attempt.automatic = 1;
    expect(requestedWeight(attempt)).toBe(1);
    attempt.requested = 2;
    expect(requestedWeight(attempt)).toBe(2);
  });
});
describe("Lifts", () => {
  let lifts: ILifts;
  beforeEach(() => (lifts = { bench: [], squat: [], deadlift: [] }));
  test("default attemptInfo", () => {
    const info = attemptInfo(lifts);
    expect(info).toStrictEqual(new AttemptInfo());
  });
  test("attemptInfo", () => {
    lifts.squat = [
      createIAttempt({ status: LiftStatus.SUCCESFUL, requested: 1 }),
      createIAttempt({ automatic: 2 })
    ];
    const info = attemptInfo(lifts);
    expect(info).toStrictEqual(
      new AttemptInfo({
        liftName: "squat",
        attemptNumberOneIndexed: 2,
        weight: 2
      })
    );
  });
});
