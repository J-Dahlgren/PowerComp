export type Id = number;
export type IEntity<T extends {}> = T & { id: Id };
