import { HotRxProp } from "./hot-rx-prop";

describe("HotRxProp", () => {
  let rProp: HotRxProp<number>;
  let val = 0;
  beforeEach(() => {
    rProp = new HotRxProp<number>(() => val);
    val = 0;
  });
  it("saves set value", () => {
    expect(rProp.val).toBe(0);
    val = 2;
    expect(rProp.val).toBe(2);
  });
  it("emits new values", done => {
    const mockFn = jest.fn();
    rProp.$.subscribe(v => mockFn(v));

    setTimeout(() => {
      expect(mockFn).toBeCalledTimes(1);
      done();
    }, 20);
    rProp.val = 1;
  });
});
