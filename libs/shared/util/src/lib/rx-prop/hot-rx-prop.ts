import { IRxProp } from "./IRxProp";
import { Subject } from "rxjs";

export class HotRxProp<T> implements IRxProp<T> {
  protected subject = new Subject<T>();
  constructor(protected accessFunc: () => T) {}
  public get $() {
    return this.subject.asObservable();
  }
  public set val(value: T) {
    this.subject.next(value);
  }
  public get val() {
    return this.accessFunc();
  }
}
