import { BehaviorSubject } from "rxjs";
import { IRxProp } from "./IRxProp";
export class RxProp<T> implements IRxProp<T> {
  protected bs: BehaviorSubject<T>;
  constructor(initial: T) {
    this.bs = new BehaviorSubject(initial);
  }
  public get val() {
    return this.bs.value;
  }
  public set val(value: T) {
    this.bs.next(value);
  }
  public get $() {
    return this.bs.asObservable();
  }
}
