import { RxProp } from "./rx-prop";

describe("RxProp", () => {
  let rProp: RxProp<number>;
  beforeEach(() => {
    rProp = new RxProp<number>(0);
  });
  it("saves set value", () => {
    expect(rProp.val).toBe(0);
    rProp.val = 1;
    expect(rProp.val).toBe(1);
  });
  it("emits new values", done => {
    const mockFn = jest.fn();
    rProp.$.subscribe(val => mockFn(val));

    setTimeout(() => {
      expect(mockFn).toBeCalledTimes(2);
      done();
    }, 20);
    rProp.val = 1;
  });
});
