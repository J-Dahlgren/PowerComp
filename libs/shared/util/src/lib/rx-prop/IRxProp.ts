import { Observable } from "rxjs";

export interface IRxProp<T> {
  readonly $: Observable<T>;
  val: T;
}
