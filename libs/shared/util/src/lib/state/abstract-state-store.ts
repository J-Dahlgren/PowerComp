import { IStateStore } from "./IStateStore";
import { BehaviorSubject, Observable } from "rxjs";
import { distinctUntilChanged } from "rxjs/operators";
import { isEqual } from "lodash";

export abstract class AbstractStateStore<T extends {}>
  implements IStateStore<T> {
  abstract get $(): Observable<T>;
  abstract get state(): T;

  abstract select<K extends keyof T>(key: K): Observable<T[K]>;
  get<K extends keyof T>(key: K): T[K] {
    return this.state[key];
  }
}
