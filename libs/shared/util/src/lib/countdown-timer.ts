import { BehaviorSubject } from "rxjs";
import { distinctUntilChanged } from "rxjs/operators";
import { RxProp } from "./rx-prop";

export type ClockStates = "PAUSED" | "RUNNING";
export interface ClockState {
  status: ClockStates;
  remainingMillis?: number;
}

export class CountdownTimer {
  public readonly status = new RxProp<ClockStates>("PAUSED");
  public readonly remainingMillis = new RxProp<number>(0);
  private interval = setTimeout(() => {}, 0); // Initialize with empty timer

  protected endTime: number = 0;
  constructor(private TickInvervalMillis: number = 100) {
    this.remainingMillis.$.subscribe(t => {
      if (t < 0) {
        this.remainingMillis.val = 0;
        this.status.val = "PAUSED";
      }
    });
    this.status.$.pipe(distinctUntilChanged()).subscribe(Status => {
      if (Status === "RUNNING") {
        this.endTime = Date.now() + this.remainingMillis.val;
        this.setInterval();
      } else {
        this.clearInterval();
      }
    });
  }
  public get state(): ClockState {
    return {
      status: this.status.val,
      remainingMillis: this.remainingMillis.val
    };
  }
  public pause(RemainingMillis?: number) {
    this.status.val = "PAUSED";
    if (typeof RemainingMillis === "number" && RemainingMillis >= 0) {
      this.remainingMillis.val = RemainingMillis;
    }
  }
  public start() {
    this.status.val = "RUNNING";
  }
  public set state(state: ClockState) {
    if (state.remainingMillis) {
      this.remainingMillis.val = state.remainingMillis;
    }
    this.status.val = state.status;
  }
  protected tick() {
    if (this.status.val === "RUNNING") {
      const t = this._remainingMillis;
      if (t <= 0) {
        this.status.val = "PAUSED";
      }
      this.remainingMillis.val = t;
    }
  }
  protected setInterval() {
    this.clearInterval();
    this.interval = setInterval(() => this.tick(), this.TickInvervalMillis);
    this.tick(); // run function once at first to avoid delay
  }
  protected clearInterval() {
    try {
      clearInterval(this.interval);
    } catch (e) {}
  }

  protected get _remainingMillis(): number {
    if (this.status.val === "PAUSED") {
      return this.remainingMillis.val;
    }
    const delta = this.endTime - Date.now();
    return delta >= 0 ? delta : 0;
  }
}
