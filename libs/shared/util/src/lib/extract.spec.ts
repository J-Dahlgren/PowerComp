import { extractKeys } from "./extract";

describe("extractKeys", () => {
  it("Returns keys", () => {
    const keys = extractKeys({ a: 1, b: "abc" });
    expect(keys).toStrictEqual(["a", "b"]);
  });
});
