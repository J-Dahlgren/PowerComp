import { EventBus } from "./event-bus";
import { filter, map, timeout } from "rxjs/operators";
import { Subject, Subscriber, Observable } from "rxjs";

export interface IRoomEvent<T> {
  room: string;
  data: T;
}
export class RoomEvent<T> implements IRoomEvent<T> {
  constructor(public room: string, public data: T) {}
}
export type RoomBus<T extends object> = {
  [key in keyof T]: IRoomEvent<T[key]>;
};

export class RoomEventBus<T extends object> extends EventBus<RoomBus<T>> {
  private roomRequests = new Subject<{
    room: string;
    receiver: Subscriber<T>;
  }>();
  onIn<K extends keyof T>(type: K, room: (() => string) | string) {
    const r = () => (typeof room === "function" ? room() : room);
    return this.on(type).pipe(
      filter(event => event.room === r()),
      map(event => event.data)
    );
  }
  emitIn<K extends keyof T>(type: K, room: string, value: T[K]) {
    this.emit(type, { room: room, data: value });
  }
  requestRoom(room: string, timeoutMillis = 100): Observable<T> {
    return new Observable<T>(subscriber => {
      this.roomRequests.next({ room, receiver: subscriber });
    }).pipe(timeout(timeoutMillis));
  }
  onRoomRequest(room: string): Observable<Subscriber<T>> {
    return this.roomRequests.asObservable().pipe(
      filter(event => event.room === room),
      map(event => event.receiver as Subscriber<T>)
    );
  }
}
