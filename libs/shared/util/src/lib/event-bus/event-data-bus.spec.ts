import { DataBus } from "./data-bus";
import { skip } from "rxjs/operators";
import { BusOptions, EventDataBus } from "./event-data-bus";

interface TestEventList {
  a: number;
  b: string;
}
describe("EventDataBus", () => {
  const options: BusOptions<TestEventList> = {
    a: { event: true },
    b: { initialValue: "1" }
  };
  let bus: EventDataBus<TestEventList>;
  beforeEach(() => (bus = new EventDataBus(options)));
  test("Emits events", done => {
    bus
      .on("a")
      .pipe()
      .subscribe(event => {
        expect(typeof event).toEqual("number");
        expect(event).toBe(10);
        done();
      });
    bus.emit("a", 10);
  });
  test("Emits latest value", done => {
    bus
      .on("b")
      .pipe(skip(1))
      .subscribe(event => {
        expect(typeof event).toEqual("string");
        expect(event).toBe("10");
        done();
      });
    bus.emit("b", "10");
  });
});
