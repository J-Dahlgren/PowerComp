import { BehaviorSubject, Observable, Subject } from "rxjs";
import { IEventBus } from "./IEventBus";

interface EventStream {
  event: true;
}
interface DataStream<T> {
  event?: false;
  initialValue: T;
}
export function isDataStream<T>(opt: DataBusOption<T>): opt is DataStream<T> {
  return !opt.event;
}
export type DataBusOption<T> = EventStream | DataStream<T>;

export type MixedBus<T extends object> = {
  [key in keyof T]: Subject<T[key]>;
};
export type BusOptions<T extends object> = {
  [key in keyof T]: DataBusOption<T[key]>;
};

export class EventDataBus<T extends object> implements IEventBus<T> {
  private bus!: MixedBus<T>;
  constructor(options: BusOptions<T>) {
    const _bus = {} as MixedBus<T>;
    for (const key in options) {
      const opt = options[key];
      _bus[key] = isDataStream(opt)
        ? new BehaviorSubject(opt.initialValue)
        : new Subject();
    }
    this.bus = _bus;
  }
  emit<K extends keyof T>(type: K, payload: T[K]) {
    this.bus[type].next(payload);
  }
  on<K extends keyof T>(type: K): Observable<T[K]> {
    return this.bus[type].asObservable();
  }
}
