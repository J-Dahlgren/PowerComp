import { RoomEventBus, RoomBus } from "./room-event-bus";
import { cloneDeep } from "lodash";
import { sleep } from "../sleep";
interface TestEventList {
  a: number;
  b: string;
  c: null;
}
describe("RoomBus", () => {
  let bus: RoomEventBus<TestEventList>;
  beforeEach(() => (bus = new RoomEventBus()));
  test("Emits events on platform", done => {
    bus.onIn("a", "room1").subscribe(value => {
      expect(value).toBe(10);
      done();
    });
    bus.emitIn("a", "room2", 11);
    bus.emitIn("c", "room1", null);
    bus.emitIn("a", "room1", 10);
  }, 100);
  test("Room requests", async () => {
    const response: TestEventList = { a: 1, b: "", c: null };
    const mock1 = jest.fn();
    const mock2 = jest.fn();
    bus.onRoomRequest("room1").subscribe(event => {
      event.next(response);
      event.complete();
    });

    bus.onRoomRequest("room2").subscribe(() => {
      mock2();
    });

    bus.requestRoom("room1").subscribe(mock1, () => {}, mock1);
    await sleep(20);
    expect(mock2).toBeCalledTimes(0);
    expect(mock1).toBeCalledTimes(2);
    expect(mock1).toHaveBeenNthCalledWith(1, cloneDeep(response));
    expect(mock1).toHaveBeenNthCalledWith(2);
  });
});
