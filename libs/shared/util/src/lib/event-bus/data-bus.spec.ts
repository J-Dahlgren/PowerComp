import { DataBus } from "./data-bus";
import { skip } from "rxjs/operators";

interface TestEventList {
  a: number;
  b: string;
}
describe("DataBus", () => {
  const intitialValues: TestEventList = { a: 1, b: "1" };
  let bus: DataBus<TestEventList>;
  beforeEach(() => (bus = new DataBus(intitialValues)));
  test("Emits events of correct type", done => {
    bus
      .on("a")
      .pipe(skip(1))
      .subscribe(event => {
        expect(typeof event).toEqual("number");
        expect(event).toBe(10);
        done();
      });
    bus.emit("a", 10);
  });
  it("Gets latest value of key", () => {
    expect(typeof bus.get("a")).toBe("number");
    expect(bus.get("a")).toBe(1);
  });
  it("Gets all values", () => {
    bus.emit("a", 12345);
    expect(bus.getAll()).toStrictEqual({ ...intitialValues, a: 12345 });
  });
  it("Listens to any data change", () => {
    bus.emit("a", 12345);
    const mock = jest.fn();
    bus.onAny().subscribe(event => mock(event));
    bus.emit("b", "abc");
    expect(mock).toHaveBeenNthCalledWith(1, { type: "a", payload: 12345 });
    expect(mock).toHaveBeenNthCalledWith(2, { type: "b", payload: "1" });
    expect(mock).toHaveBeenNthCalledWith(3, { type: "b", payload: "abc" });
  });
});
