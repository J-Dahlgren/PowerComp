import { BehaviorSubject, Observable, merge } from "rxjs";
import { IEventBus } from "./IEventBus";
import { extractKeys } from "../extract";
import { map } from "rxjs/operators";

export type BehaviourBus<T extends object> = {
  [key in keyof T]: BehaviorSubject<T[key]>;
};

export class DataBus<T extends object> implements IEventBus<T> {
  private bus!: BehaviourBus<T>;
  constructor(initialValues: T) {
    const _bus = {} as BehaviourBus<T>;
    for (const key in initialValues) {
      _bus[key] = new BehaviorSubject(initialValues[key]);
    }
    this.bus = _bus;
  }
  emit<K extends keyof T>(type: K, payload: T[K]) {
    this.bus[type].next(payload);
  }
  on<K extends keyof T>(type: K): Observable<T[K]> {
    return this.bus[type].asObservable();
  }
  get<K extends keyof T>(type: K): T[K] {
    return this.bus[type].value;
  }
  getAll() {
    const obj: T = {} as T;
    extractKeys(this.bus).forEach(key => (obj[key] = this.get(key)));
    return obj;
  }
  onAny() {
    const streams = extractKeys(this.bus).map(key =>
      this.bus[key].asObservable().pipe(
        map(value => ({
          type: key,
          payload: value
        }))
      )
    );
    return merge(...streams);
  }
}
