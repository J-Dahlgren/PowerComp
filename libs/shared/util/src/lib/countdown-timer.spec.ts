import { CountdownTimer } from "./countdown-timer";
import { sleep } from "./sleep";
describe("CountdownTimer", () => {
  it("Starts clock on state change", async () => {
    const clock = new CountdownTimer(50);
    clock.remainingMillis.val = 1000;
    clock.start();
    await sleep(150);
    expect(clock.remainingMillis.val).toBeLessThanOrEqual(900);
    clock.pause(100);
    clock.status.val = "RUNNING";
    await sleep(150);
    expect(clock.remainingMillis.val).toBe(0);
    expect(clock.status.val).toBe("PAUSED");
    clock.state = { status: "RUNNING", remainingMillis: 100 };
    await sleep(50);
    expect(clock.state.status).toBe("RUNNING");
    clock.pause();
  }, 500);
});
