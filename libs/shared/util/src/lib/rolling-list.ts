import { cloneDeep } from "lodash";

export class RollingList<T> {
  private list: T[] = [];
  constructor(private capacity: number) {
    if (capacity < 0) {
      throw new Error("Capacity can't be below 0");
    }
  }
  add(...items: T[]) {
    this.list.push(...items);
    this.shift();
  }
  getList() {
    return cloneDeep(this.list);
  }
  private shift() {
    while (this.list.length > this.capacity) {
      this.list.shift();
    }
  }
}
