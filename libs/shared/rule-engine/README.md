# rule-engine

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `ng test rule-engine` to execute the unit tests via [Jest](https://jestjs.io).
