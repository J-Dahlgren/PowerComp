import { LiftOrderCompareFunction } from "./lift-order-compare-function";
import { ILifter, attemptInfo } from "@pc/shared/data-types";

export function NextLifterDeterminator(
  lifters: ILifter[],
  comparator: LiftOrderCompareFunction
) {
  const sorted = lifters.sort(comparator);
  const filtered = sorted.filter(
    lifter => attemptInfo(lifter.lifts).liftName !== "done"
  );
  let currentLifter: null | ILifter = null;
  let nextLifter: null | ILifter = null;
  if (filtered.length > 0) {
    currentLifter = filtered[0];
  }
  if (filtered.length > 1) {
    nextLifter = filtered[1];
  }
  return {
    liftOrder: sorted,
    currentLifterInfo: currentLifter,
    nextLifterInfo: nextLifter
  };
}
