import { ClassicCompareFunction } from "./classic-compare-function";
import { LiftOrderCompareFunction } from "./lift-order-compare-function";
import { CompetitionModes } from "./competition-modes";
type liftOrderFunctions = {
  [key in CompetitionModes]: LiftOrderCompareFunction;
};
export const LiftOrderFunctions: liftOrderFunctions = {
  CLASSIC: ClassicCompareFunction
};
