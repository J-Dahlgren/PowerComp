import { ILifter } from "@pc/shared/data-types";
export type LiftOrderCompareFunction = (a: ILifter, b: ILifter) => number;
