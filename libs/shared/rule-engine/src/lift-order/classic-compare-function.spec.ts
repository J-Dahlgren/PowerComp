import {
  LiftStatus,
  ILifter,
  createIAttempt,
  createILifter
} from "@pc/shared/data-types";
import { ClassicCompareFunction } from "./classic-compare-function";

describe("ClassicCompareFunction", () => {
  let A: ILifter;
  let B: ILifter;

  beforeEach(() => {
    A = createILifter({ lot: 1 });
    B = createILifter({ lot: 2 });
  });
  test("sorts on lift type", () => {
    A.lifts.bench = [createIAttempt({ requested: 100 })];
    B.lifts.squat = [createIAttempt({ requested: 100 })];
    expect(ClassicCompareFunction(A, B)).toBe(1);
  });
  test("sorts on lift weight", () => {
    A.lifts.bench = [createIAttempt({ requested: 100 })];
    B.lifts.bench = [createIAttempt({ requested: 110 })];
    expect(ClassicCompareFunction(A, B)).toBe(-10);
  });
  test("sorts on attempt index", () => {
    A.lifts.bench = [
      createIAttempt({ requested: 100, status: LiftStatus.SUCCESFUL }),
      createIAttempt({ automatic: 110 })
    ];
    B.lifts.bench = [createIAttempt({ requested: 100 }), createIAttempt()];
    expect(ClassicCompareFunction(A, B)).toBe(1);
  });
  test("sorts on start", () => {
    expect(ClassicCompareFunction(A, B)).toBe(-1);
  });
});
