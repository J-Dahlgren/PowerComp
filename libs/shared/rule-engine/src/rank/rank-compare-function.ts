import { ILifter } from "@pc/shared/data-types";

export type RankCompareFunction = (a: ILifter, b: ILifter) => number;
