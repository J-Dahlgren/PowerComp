import { StateStore, CountdownTimer } from "@pc/shared/util";
export class PlatformStateService {
  protected data = new StateStore({});
  protected clock = new CountdownTimer();
  protected decision = new StateStore<number[]>([]);
}
