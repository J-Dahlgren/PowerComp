import { RoomEventBus } from "@pc/shared/util";
import {
  PlatformDataEvents,
  PlatformClientEvents,
  PlatformServerEvents
} from "@pc/shared/events";
import { Injectable, Logger } from "@nestjs/common";
@Injectable()
export class PlatformEventService {
  public readonly clientEvents = new RoomEventBus<PlatformClientEvents>();
  public readonly serverEvents = new RoomEventBus<PlatformServerEvents>();
  public readonly dataEvents = new RoomEventBus<PlatformDataEvents>();
  constructor(private logger: Logger) {
    logger.setContext(PlatformEventService.name);
    logger.debug("Created");
    this.dataEvents
      .onAny()
      .subscribe(event =>
        logger.verbose(
          `${event.type}: ${JSON.stringify(event.payload.data, null, 2)}`,
          `${PlatformEventService.name} - ${event.payload.room}`
        )
      );
  }
}
