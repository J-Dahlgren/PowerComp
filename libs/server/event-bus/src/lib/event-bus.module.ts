import { Module } from "@nestjs/common";
import { LoggerModule } from "@pc/logger";
import { GlobalEventService } from "./global-event.service";
import { PlatformEventService } from "./platform-event.service";
@Module({
  imports: [LoggerModule],
  providers: [GlobalEventService, PlatformEventService],
  exports: [GlobalEventService, PlatformEventService]
})
export class EventBusModule {}
