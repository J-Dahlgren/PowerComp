import { Injectable, Logger } from "@nestjs/common";
import { DataBus } from "@pc/shared/util";
import { GlobalEvents } from "@pc/shared/events";
@Injectable()
export class GlobalEventService extends DataBus<GlobalEvents> {
  constructor(private logger: Logger) {
    super(new GlobalEvents());
    logger.setContext(GlobalEventService.name);
    logger.debug("Created");
    this.onAny().subscribe(event =>
      logger.verbose(`${event.type}: ${JSON.stringify(event.payload, null, 2)}`)
    );
  }
}
