import {
  WebSocketGateway,
  WebSocketServer,
  OnGatewayInit,
  SubscribeMessage,
  ConnectedSocket,
  MessageBody,
  OnGatewayConnection
} from "@nestjs/websockets";
import { Server, Socket } from "socket.io";
import { Logger, OnModuleDestroy } from "@nestjs/common";
import {
  LEAVE,
  JOIN,
  SYNC,
  platformEvent,
  globalEvent,
  GLOBAL_DATA,
  PLATFORM_DATA_EVENT,
  PLATFORM_CLIENT_EVENT,
  PlatformClientEvents,
  PLATFORM_SERVER_EVENT
} from "@pc/shared/events";
import { SubSink } from "subsink";
import { GlobalEventService, PlatformEventService } from "@pc/server/event-bus";
import { take } from "rxjs/operators";
import {
  extractKeys,
  RoomEvent,
  DataEvent,
  IEvent,
  RoomBus
} from "@pc/shared/util";

@WebSocketGateway()
export class SocketGateway implements OnGatewayInit<Server>, OnModuleDestroy {
  private subs = new SubSink();
  constructor(
    private logger: Logger,
    private globEvents: GlobalEventService,
    private platformEvents: PlatformEventService
  ) {
    logger.setContext(SocketGateway.name);
    logger.debug("Created");
  }

  @WebSocketServer() server!: Server;
  afterInit(server: Server) {
    this.logger.debug("afterInit");
    this.subs.sink = this.globEvents
      .onAny()
      .subscribe(event => this.server.emit(GLOBAL_DATA, event));
    // REMOVE
    // this.subs.sink = this.platformEvents
    //   .onAny()
    //   .subscribe(event =>
    //     this.server.in(event.payload.room).emit(PLATFORM_DATA_EVENT, event)
    //   );
    //
    this.subs.sink = this.platformEvents.dataEvents
      .onAny()
      .subscribe(event =>
        this.server.in(event.payload.room).emit(PLATFORM_DATA_EVENT, event)
      );
    this.subs.sink = this.platformEvents.serverEvents
      .onAny()
      .subscribe(event =>
        this.server.in(event.payload.room).emit(PLATFORM_SERVER_EVENT, event)
      );
  }
  @SubscribeMessage(PLATFORM_CLIENT_EVENT)
  platformClientEvent(event: IEvent<RoomBus<PlatformClientEvents>>) {
    const { type, payload } = event;
    this.platformEvents.clientEvents.emit(type, payload);
  }
  @SubscribeMessage(platformEvent(LEAVE))
  leaveRoom(@MessageBody() room: string, @ConnectedSocket() socket: Socket) {
    this.logger.debug(`${socket.id}: ${platformEvent(LEAVE)} ${room}`);
    socket.leave(room);
  }
  @SubscribeMessage(platformEvent(JOIN))
  joinRoom(@MessageBody() room: string, @ConnectedSocket() socket: Socket) {
    this.logger.debug(`${socket.id}: ${platformEvent(JOIN)} ${room}`);
    socket.join(room);
    this.platformEvents.dataEvents
      .requestRoom(room, 100)
      .pipe(take(1))
      .subscribe(
        data =>
          extractKeys(data).forEach(key =>
            socket.emit(PLATFORM_DATA_EVENT, new RoomEvent(room, data[key]))
          ),
        error => this.logger.warn(`Platform ${room} request error: ${error}`)
      );
  }
  @SubscribeMessage(globalEvent(SYNC))
  sync(@ConnectedSocket() socket: Socket) {
    const allData = this.globEvents.getAll();
    extractKeys(allData).forEach(key =>
      socket.emit(GLOBAL_DATA, new DataEvent(key, allData[key]))
    );
  }
  onModuleDestroy() {
    this.subs.unsubscribe();
  }
}
