import { Module } from "@nestjs/common";
import { LoggerModule } from "@pc/logger";
import { SocketGateway } from "./socket.gateway";
import { EventBusModule } from "@pc/server/event-bus";
@Module({
  imports: [LoggerModule, EventBusModule],
  providers: [SocketGateway],
  exports: [SocketGateway]
})
export class SocketModule {}
