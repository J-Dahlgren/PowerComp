# server-competition-manager

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `ng test server-competition-manager` to execute the unit tests via [Jest](https://jestjs.io).
