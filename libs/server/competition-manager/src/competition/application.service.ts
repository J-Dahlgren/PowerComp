import {
  Injectable,
  Logger,
  OnModuleInit,
  OnModuleDestroy
} from "@nestjs/common";
import { ModuleRef } from "@nestjs/core";
import {
  PlatformEntityService,
  CompetitionEntityService
} from "@pc/data-access";
import { PlatformManagerService } from "./platform-manager.service";
import { SubSink } from "subsink";
import { Platform } from "@pc/entity";
import { remove } from "lodash";
import { interval } from "rxjs";
import { auditTime } from "rxjs/operators";
import { GlobalEventService } from "@pc/server/event-bus";
@Injectable()
export class ApplicationService implements OnModuleInit, OnModuleDestroy {
  private subs = new SubSink();
  private platforms: PlatformManagerService[] = [];
  constructor(
    private moduleRef: ModuleRef,
    private logger: Logger,
    private pService: PlatformEntityService,
    private cService: CompetitionEntityService,
    private globalEventService: GlobalEventService
  ) {
    logger.setContext(ApplicationService.name);
    logger.debug("Created");
  }

  onModuleInit() {
    this.pService
      .find()
      .then(platforms =>
        platforms.forEach(platform => this.createPlatformService(platform))
      );
    this.subs.sink = this.pService.stream.onInsert.subscribe(inserted =>
      this.createPlatformService(inserted)
    );
    this.subs.sink = this.pService.stream.onRemove
      .pipe(auditTime(500)) // Platforms are self-terminating, just need an audit to remove in chunks
      .subscribe(() => this.cleanPlatforms());
    this.subs.sink = this.cService.stream.onAnyChange().subscribe(async () => {
      const competitions = await this.cService.find();
      this.globalEventService.emit("competitions", competitions);
    });
    this.subs.sink = this.pService.stream.onAnyChange().subscribe(async () => {
      const platforms = await this.pService.find();
      this.globalEventService.emit("platforms", platforms);
    });
  }
  onModuleDestroy() {
    this.subs.unsubscribe();
  }
  private async createPlatformService(p: Platform) {
    const service = await this.moduleRef.create(PlatformManagerService);
    service.onInit(p);
    this.platforms.push(service);
  }
  private cleanPlatforms() {
    remove(this.platforms, p => p.TERMINATED);
    this.logger.debug(`${this.platforms.length} platforms active`);
  }
}
