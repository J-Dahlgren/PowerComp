import { Logger } from "@nestjs/common";
import { SubSink } from "subsink";
import { CrudService } from "@pc/data-access";
import { filter } from "rxjs/operators";
import { IEntity } from "@pc/shared/data-types";
export enum DynamicServiceStatus {
  NOT_INITIALIZED,
  ACTIVE,
  TERMINATED
}
export abstract class EntitySessionService<T extends IEntity<{}>> {
  protected entity!: T;
  protected status: DynamicServiceStatus = DynamicServiceStatus.NOT_INITIALIZED;
  protected subs = new SubSink();
  constructor(
    protected readonly entityService: CrudService<T>,
    protected readonly logger: Logger,
    protected readonly concreteName: string
  ) {
    logger.setContext(concreteName);
  }
  public get TERMINATED() {
    return this.status === DynamicServiceStatus.TERMINATED;
  }
  protected abstract afterInit(): void;
  onInit(entity: T) {
    if (this.status === DynamicServiceStatus.ACTIVE) {
      this.logger.error(
        `Entity initialized again, was: ${this.entity.id}, new: ${entity.id}`
      );
      return;
    }
    if (!this.entity) {
      this.logger.setContext(`${this.concreteName} #${entity.id}`);
      this.logger.debug("Created");
      this.entity = entity;
      this.status = DynamicServiceStatus.ACTIVE;

      this.subs.sink = this.entityService.stream.onUpdate
        .pipe(filter(e => e.id === this.entity?.id))
        .subscribe(e => (this.entity = e));

      this.subs.sink = this.entityService.stream.onRemove
        .pipe(filter(e => e.id === this.entity?.id))
        .subscribe(() => this.terminate());
      this.afterInit();
    }
  }
  terminate() {
    if (this.status === DynamicServiceStatus.ACTIVE) {
      this.logger.debug(`Terminated`);
      this.subs.unsubscribe();
      this.status = DynamicServiceStatus.TERMINATED;
    }
  }
}
