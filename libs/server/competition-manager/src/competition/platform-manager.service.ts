import { Injectable, Scope, Logger } from "@nestjs/common";
import { PlatformEntityService } from "@pc/data-access";
import { Platform } from "@pc/entity";
import { EntitySessionService } from "./dynamic-session.service";
import { PlatformEventService } from "@pc/server/event-bus";
import { StateStore } from "@pc/shared/util";
import { PlatformData } from "@pc/shared/events";
import { auditTime } from "rxjs/operators";

@Injectable({ scope: Scope.TRANSIENT })
export class PlatformManagerService extends EntitySessionService<Platform> {
  protected platform = new StateStore<PlatformData>(new PlatformData());
  constructor(
    private eventBus: PlatformEventService,
    entityService: PlatformEntityService,
    logger: Logger
  ) {
    super(entityService, logger, PlatformManagerService.name);
  }
  protected get room() {
    return `${this.entity?.id}` || "-1";
  }
  afterInit() {
    this.subs.sink = this.eventBus.dataEvents
      .onRoomRequest(`${this.entity.id}`)
      .subscribe(requester => {
        requester.next({ data: this.platform.state });
      });
    this.subs.sink = this.platform.$.pipe(auditTime(500)).subscribe(state =>
      this.eventBus.dataEvents.emitIn("data", this.room, state)
    );
  }
}
