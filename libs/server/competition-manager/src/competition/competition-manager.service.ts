import { Injectable, Scope, Logger } from "@nestjs/common";
import { CompetitionEntityService } from "@pc/data-access";
import { Competition } from "@pc/entity";
import { EntitySessionService } from "./dynamic-session.service";

@Injectable({ scope: Scope.TRANSIENT })
export class CompetitionManagerService extends EntitySessionService<
  Competition
> {
  constructor(entityService: CompetitionEntityService, logger: Logger) {
    super(entityService, logger, CompetitionManagerService.name);
  }
  protected afterInit(): void {}
}
