import { Module } from "@nestjs/common";
import { PlatformModule, CompetitionModule } from "@pc/data-access";
import { EventBusModule } from "@pc/server/event-bus";
import { LoggerModule } from "@pc/logger";
import { ApplicationService } from "./application.service";
@Module({
  imports: [CompetitionModule, PlatformModule, LoggerModule, EventBusModule],
  controllers: [],
  providers: [ApplicationService],
  exports: [ApplicationService]
})
export class CompetitionManagerModule {}
