import { Module, DynamicModule } from "@nestjs/common";

@Module({
  controllers: [],
  providers: []
})
export class AuthModule {
  static forRoot(): DynamicModule {
    return {
      module: AuthModule,
      imports: [],
      controllers: [],
      exports: [],
      providers: []
    };
  }
}
