import { Controller, Post, Body } from "@nestjs/common";
import { LoginDto } from "@pc/shared/auth";
@Controller("auth")
export class AuthController {
  @Post("login")
  login(@Body() body: LoginDto) {}
}
