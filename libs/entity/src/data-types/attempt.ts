import { IAttempt, LiftStatus } from "@pc/shared/data-types";
import { IsEnum, IsOptional, IsNumber } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class Attempt implements IAttempt {
  constructor(initial: Partial<IAttempt> = {}) {
    Object.assign(this, initial);
  }
  @IsEnum(LiftStatus)
  @ApiProperty({ enum: LiftStatus })
  status: LiftStatus = LiftStatus.NOT_ATTEMPTED;

  @IsOptional()
  @IsNumber()
  @ApiProperty({ required: false })
  automatic?: number;

  @IsOptional()
  @IsNumber()
  @ApiProperty({ required: false })
  requested?: number;

  requestedWeight() {
    return this.requested || this.automatic;
  }
}
