import { IsNumber, Min, ValidateNested } from "class-validator";
import { Type } from "class-transformer";
import { ApiProperty } from "@nestjs/swagger";
import { IPlatformWeights, IWeightPlate } from "@pc/shared/data-types";
export class PlatformWeights implements IPlatformWeights {
  @IsNumber({ allowInfinity: false, allowNaN: false })
  @Min(0)
  @ApiProperty()
  collarWeight!: number;

  @ValidateNested({ each: true })
  @Type(() => WeightPlate)
  @ApiProperty({ type: () => [WeightPlate] })
  plates!: WeightPlate[];
}
export class WeightPlate implements IWeightPlate {
  @IsNumber({ allowInfinity: false, allowNaN: false })
  @Min(0)
  @ApiProperty()
  weight!: number;

  @Min(0)
  @IsNumber({ allowInfinity: false, allowNaN: false })
  @ApiProperty()
  quantity!: number;
}
