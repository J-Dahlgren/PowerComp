import { EntitySubscriber } from "./entity.subscriber";

import { EventSubscriber, Connection } from "typeorm";
import { Lifter } from "../entity";
@EventSubscriber()
export class LifterSubscriber extends EntitySubscriber<Lifter> {
  constructor(connection: Connection) {
    super();
    connection.subscribers.push(this);
  }
  listenTo(): string | Function {
    return Lifter;
  }
}
