import { EntitySubscriber } from "./entity.subscriber";
import { EventSubscriber, Connection } from "typeorm";
import { Competition } from "../entity";
@EventSubscriber()
export class CompetitionSubscriber extends EntitySubscriber<Competition> {
  constructor(connection: Connection) {
    super();
    connection.subscribers.push(this);
  }
  listenTo(): string | Function {
    return Competition;
  }
}
