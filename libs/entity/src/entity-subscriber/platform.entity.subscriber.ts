import { EntitySubscriber } from "./entity.subscriber";

import { EventSubscriber, Connection } from "typeorm";
import { Platform } from "../entity";
@EventSubscriber()
export class PlatformSubscriber extends EntitySubscriber<Platform> {
  constructor(connection: Connection) {
    super();
    connection.subscribers.push(this);
  }
  listenTo(): string | Function {
    return Platform;
  }
}
