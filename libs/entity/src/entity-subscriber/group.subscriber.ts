import { EventSubscriber, Connection } from "typeorm";
import { EntitySubscriber } from "./entity.subscriber";
import { Group } from "../entity";

@EventSubscriber()
export class GroupSubscriber extends EntitySubscriber<Group> {
  constructor(connection: Connection) {
    super();
    connection.subscribers.push(this);
  }
  listenTo(): string | Function {
    return Group;
  }
}
