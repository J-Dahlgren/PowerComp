import {
  EntitySubscriberInterface,
  InsertEvent,
  UpdateEvent,
  RemoveEvent
} from "typeorm";
import { Subject, Observable, merge } from "rxjs";
import { filter } from "rxjs/operators";
export interface IEntitySubscriber<T extends object> {
  readonly onInsert: Observable<T>;
  readonly onUpdate: Observable<T>;
  readonly onRemove: Observable<T>;
  onAnyChange(filterFunc?: (event: T) => boolean): Observable<T>;
  watch<K extends keyof T>(
    key: K,
    value: () => T[K] | undefined
  ): Observable<T>;
}
export abstract class EntitySubscriber<T extends {}>
  implements EntitySubscriberInterface<T>, IEntitySubscriber<T> {
  private insertStream = new Subject<T>();
  private updateStream = new Subject<T>();
  private removeStream = new Subject<T>();
  abstract listenTo(): Function | string;
  public get onInsert() {
    return this.insertStream.asObservable();
  }
  public get onUpdate() {
    return this.updateStream.asObservable();
  }
  public get onRemove() {
    return this.removeStream.asObservable();
  }
  watch<K extends keyof T>(key: K, value: () => T[K] | undefined) {
    return this.onAnyChange(entity => entity[key] === value());
  }
  onAnyChange(filterFunc: (event: T) => boolean = () => true) {
    return merge(this.onInsert, this.onUpdate, this.onRemove).pipe(
      filter(filterFunc)
    );
  }
  afterInsert(event: InsertEvent<T>) {
    this.insertStream.next(event.entity);
  }
  afterUpdate(event: UpdateEvent<T>) {
    this.updateStream.next(event.databaseEntity);
  }
  afterRemove(event: RemoveEvent<T>) {
    this.removeStream.next(event.databaseEntity);
  }
}
