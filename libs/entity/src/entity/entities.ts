import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  ManyToOne
} from "typeorm";

import { Type } from "class-transformer";
import {
  ValidateNested,
  IsNumber,
  IsString,
  IsOptional
} from "class-validator";
import { Lifts, PlatformWeights } from "../data-types";
import {
  ICompetition,
  IPlatform,
  IGroup,
  ILifter,
  Gender,
  IEntity
} from "@pc/shared/data-types";
import { ApiProperty } from "@nestjs/swagger";

@Entity()
export class Competition implements IEntity<ICompetition> {
  @PrimaryGeneratedColumn()
  id!: number;
  @ApiProperty()
  @Column({ nullable: false })
  name!: string;

  @ApiProperty({ required: false })
  @Column({ nullable: true })
  city?: string;

  @ApiProperty({ required: false })
  @Column({ nullable: true })
  location?: string;

  /* Relations */

  @Type(() => Platform)
  @OneToMany(
    () => Platform,
    platform => platform.competition,
    { cascade: ["insert", "update"] }
  )
  @ApiProperty({ required: false, type: () => [Platform] })
  platforms!: Platform[];

  @Type(() => Group)
  @OneToMany(
    () => Group,
    group => group.competition,
    { cascade: ["insert", "update"] }
  )
  groups!: Group[];
  @Type(() => Lifter)
  @OneToMany(
    () => Lifter,
    lifter => lifter.competition,
    { cascade: ["insert", "update"] }
  )
  lifters!: Lifter[];
}
@Entity()
export class Platform implements IEntity<IPlatform> {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column()
  @ApiProperty()
  competitionId!: number;

  @Column({ nullable: false })
  @ApiProperty()
  name!: string;
  @Column("simple-json")
  @ApiProperty({ type: PlatformWeights })
  @Type(() => PlatformWeights)
  weights!: PlatformWeights;

  /* Relations */

  @OneToMany(
    () => Group,
    group => group.platform,
    {}
  )
  @ApiProperty({ required: false, type: () => [Group] })
  @Type(() => Group)
  groups!: Group[];

  @ManyToOne(
    () => Competition,
    competition => competition.platforms,
    {
      cascade: [],
      onDelete: "RESTRICT",
      onUpdate: "CASCADE"
    }
  )
  competition!: Competition;
}
@Entity()
export class Group implements IEntity<IGroup> {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column()
  @ApiProperty()
  name!: string;

  @Column({ nullable: true })
  @ApiProperty({ required: false })
  weighInTime?: Date;

  @Column({ nullable: true })
  @ApiProperty({ required: false })
  competitionTime?: Date;

  /* Relations */

  @ManyToOne(
    () => Competition,
    competition => competition.groups
  )
  competition!: Competition;

  @ManyToOne(
    () => Platform,
    platform => platform.groups,
    {
      onDelete: "RESTRICT"
    }
  )
  @Type(() => Platform)
  platform!: Platform;

  @OneToMany(
    () => Lifter,
    lifter => lifter.group
  )
  lifters!: Lifter[];
}
@Entity()
export class Lifter implements IEntity<ILifter> {
  constructor(initial: Partial<IEntity<ILifter>> = {}) {
    Object.assign(this, initial);
  }
  @PrimaryGeneratedColumn()
  id!: number;

  @Column()
  @IsNumber()
  @ApiProperty()
  lot!: number;

  @Column()
  @IsNumber()
  @ApiProperty()
  competitionId!: number;

  @Column({ nullable: true })
  @ApiProperty({ required: false })
  groupId?: number;

  @Column("simple-json")
  @ValidateNested()
  @Type(() => Lifts)
  @ApiProperty({ type: Lifts })
  lifts!: Lifts;

  @Column({ nullable: true })
  @IsOptional()
  @IsString()
  @ApiProperty({ required: false })
  license?: string;

  @Column()
  @IsString()
  firstname!: string;

  @Column()
  @IsString()
  @ApiProperty()
  lastname!: string;

  @Column("simple-enum", { enum: Gender })
  @ApiProperty({ enum: Gender })
  gender!: Gender;

  @Column("date")
  @ApiProperty()
  born?: Date;

  /* Relations */

  @ManyToOne(
    () => Competition,
    competition => competition.lifters
  )
  competition!: Competition;

  @ManyToOne(
    () => Group,
    group => group.lifters
  )
  group?: Group;
}
