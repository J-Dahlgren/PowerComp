import { Injectable, Scope, Logger } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { TypeOrmCrudService } from "@nestjsx/crud-typeorm";
import { Platform, PlatformSubscriber } from "@pc/entity";
import { Repository } from "typeorm";
import { CrudService } from "../base/crud.service";
@Injectable({
  scope: Scope.DEFAULT
})
export class PlatformEntityService extends CrudService<Platform> {
  constructor(
    @InjectRepository(Platform) repo: Repository<Platform>,
    logger: Logger,
    sub: PlatformSubscriber
  ) {
    super(repo, sub, logger, PlatformEntityService.name);
  }
}
