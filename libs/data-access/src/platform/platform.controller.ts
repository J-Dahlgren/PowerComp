import { Controller, Module, Scope } from "@nestjs/common";
import { CrudController, Crud } from "@nestjsx/crud";
import { PlatformEntityService } from "./platform.service";
import { Platform } from "@pc/entity";
import { ApiTags } from "@nestjs/swagger";
@Crud({
  model: {
    type: Platform
  },
  query: {
    join: {
      groups: {}
    }
  }
})
@ApiTags("Platform")
@Controller("platform")
export class PlatformController implements CrudController<Platform> {
  constructor(public service: PlatformEntityService) {}
}
