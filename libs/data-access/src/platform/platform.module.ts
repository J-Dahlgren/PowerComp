import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Platform, PlatformSubscriber } from "@pc/entity";
import { PlatformController } from "./platform.controller";
import { PlatformEntityService } from "./platform.service";
import { LoggerModule } from "@pc/logger";

@Module({
  imports: [LoggerModule, TypeOrmModule.forFeature([Platform])],
  controllers: [PlatformController],
  providers: [PlatformEntityService, PlatformSubscriber],
  exports: [PlatformEntityService, PlatformSubscriber]
})
export class PlatformModule {}
