import { TypeOrmCrudService } from "@nestjsx/crud-typeorm";
import { Repository } from "typeorm";
import { Logger } from "@nestjs/common";
import { IEntitySubscriber } from "@pc/entity";
import { IEntity } from "@pc/shared/data-types";

export class CrudService<T extends IEntity<{}>> extends TypeOrmCrudService<T> {
  constructor(
    repo: Repository<T>,
    public readonly stream: IEntitySubscriber<T>,
    protected readonly logger: Logger,
    context: string
  ) {
    super(repo);
    logger.setContext(context);
    logger.debug("Created");
    stream.onInsert.subscribe(entity =>
      logger.verbose(`Inserted: ${entity.id}`)
    );
    stream.onUpdate.subscribe(entity =>
      logger.verbose(`Updated:  ${entity.id}`)
    );
    stream.onRemove.subscribe(entity =>
      logger.verbose(`Removed:  ${entity.id}`)
    );
  }
}
