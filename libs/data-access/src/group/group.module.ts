import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Group, GroupSubscriber } from "@pc/entity";
import { GroupController } from "./group.controller";
import { GroupEntityService } from "./group.service";
import { LoggerModule } from "@pc/logger";

@Module({
  imports: [LoggerModule, TypeOrmModule.forFeature([Group])],
  controllers: [GroupController],
  providers: [GroupEntityService, GroupSubscriber],
  exports: [GroupEntityService, GroupSubscriber]
})
export class GroupModule {}
