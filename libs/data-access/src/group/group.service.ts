import { Injectable, Scope, Logger } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { TypeOrmCrudService } from "@nestjsx/crud-typeorm";
import { Group, GroupSubscriber } from "@pc/entity";
import { Repository } from "typeorm";
import { CrudService } from "../base/crud.service";
@Injectable({
  scope: Scope.DEFAULT
})
export class GroupEntityService extends CrudService<Group> {
  constructor(
    @InjectRepository(Group) repo: Repository<Group>,
    logger: Logger,
    sub: GroupSubscriber
  ) {
    super(repo, sub, logger, GroupEntityService.name);
  }
}
