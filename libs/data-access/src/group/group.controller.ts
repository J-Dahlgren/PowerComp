import { Controller } from "@nestjs/common";
import { CrudController, Crud } from "@nestjsx/crud";
import { GroupEntityService } from "./group.service";
import { Group } from "@pc/entity";
import { ApiTags } from "@nestjs/swagger";
@Crud({
  model: {
    type: Group
  }
})
@ApiTags("Group")
@Controller("group")
export class GroupController implements CrudController<Group> {
  constructor(public service: GroupEntityService) {}
}
