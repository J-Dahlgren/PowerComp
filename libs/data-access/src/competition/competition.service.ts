import { Injectable, Scope, Logger } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { TypeOrmCrudService } from "@nestjsx/crud-typeorm";
import { Competition, CompetitionSubscriber } from "@pc/entity";
import { Repository } from "typeorm";
import { CrudService } from "../base/crud.service";
@Injectable({
  scope: Scope.DEFAULT
})
export class CompetitionEntityService extends CrudService<Competition> {
  constructor(
    @InjectRepository(Competition) repo: Repository<Competition>,
    logger: Logger,
    sub: CompetitionSubscriber
  ) {
    super(repo, sub, logger, CompetitionEntityService.name);
  }
}
