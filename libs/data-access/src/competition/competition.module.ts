import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Competition, CompetitionSubscriber } from "@pc/entity";
import { CompetitionController } from "./competition.controller";
import { CompetitionEntityService } from "./competition.service";
import { LoggerModule } from "@pc/logger";

@Module({
  imports: [LoggerModule, TypeOrmModule.forFeature([Competition])],
  controllers: [CompetitionController],
  providers: [CompetitionEntityService, CompetitionSubscriber],
  exports: [CompetitionEntityService]
})
export class CompetitionModule {}
