import { Controller, Module, Scope } from "@nestjs/common";
import { CrudController, Crud } from "@nestjsx/crud";
import { Competition } from "@pc/entity";

import { CompetitionEntityService } from "./competition.service";
import { ApiTags } from "@nestjs/swagger";
@Crud({
  model: {
    type: Competition
  },
  query: {
    join: {
      platforms: {},
      groups: {},
      lifters: {}
    }
  }
})
@ApiTags("Competition")
@Controller("competition")
export class CompetitionController implements CrudController<Competition> {
  constructor(public service: CompetitionEntityService) {}
}
