import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Lifter, LifterSubscriber } from "@pc/entity";
import { LifterController } from "./lifter.controller";
import { LifterEntityService } from "./lifter.service";
import { LoggerModule } from "@pc/logger";

@Module({
  imports: [LoggerModule, TypeOrmModule.forFeature([Lifter])],
  controllers: [LifterController],
  providers: [LifterEntityService, LifterSubscriber],
  exports: [LifterEntityService, LifterSubscriber]
})
export class LifterModule {}
