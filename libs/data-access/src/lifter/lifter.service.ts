import { Injectable, Scope, Logger } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { TypeOrmCrudService } from "@nestjsx/crud-typeorm";
import { Lifter, LifterSubscriber } from "@pc/entity";
import { Repository } from "typeorm";
import { CrudService } from "../base/crud.service";
@Injectable({
  scope: Scope.DEFAULT
})
export class LifterEntityService extends CrudService<Lifter> {
  constructor(
    @InjectRepository(Lifter) repo: Repository<Lifter>,
    logger: Logger,
    sub: LifterSubscriber
  ) {
    super(repo, sub, logger, LifterEntityService.name);
  }
}
