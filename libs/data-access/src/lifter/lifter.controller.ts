import { Controller, Module, Scope } from "@nestjs/common";
import { CrudController, Crud } from "@nestjsx/crud";
import { LifterEntityService } from "./lifter.service";
import { Lifter } from "@pc/entity";
import { ApiTags } from "@nestjs/swagger";
@Crud({
  model: {
    type: Lifter
  }
})
@ApiTags("Lifter")
@Controller("lifter")
export class LifterController implements CrudController<Lifter> {
  constructor(public service: LifterEntityService) {}
}
