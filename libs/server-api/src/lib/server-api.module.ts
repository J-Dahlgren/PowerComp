import { Module } from "@nestjs/common";
import {
  CompetitionModule,
  PlatformModule,
  GroupModule,
  LifterModule
} from "@pc/data-access";
@Module({
  imports: [CompetitionModule, PlatformModule, GroupModule, LifterModule],
  controllers: [],
  providers: [],
  exports: [CompetitionModule, PlatformModule, GroupModule, LifterModule]
})
export class ServerApiModule {}
